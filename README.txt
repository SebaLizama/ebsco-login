LOGIN EBSCO APP

El archivo de creación de Base de datos se encuentra en user.sql

El sistema cuenta con un backend y un frontend, cada uno separado en carpetas.

Dependiendo de las credenciales de base de datos se debe modificar el archivo Main.js
Datos de ingreso actuales : 
- host: 'localhost',
- user: 'root',
- password: 'jAQFjLJo3XGHnyDo',
- database: 'loginapp'

Para ejecutar la aplicación se debe ingresar a la carpeta backend y ejecutar el comando 
- npm start
El cual permite iniciar el servidor

La aplicación se ejecuta de forma local en el puerto 3000

http://localhost:3000/

Usuario de test en el script de BD 
- user: Sebastian 
- pass: 1234
